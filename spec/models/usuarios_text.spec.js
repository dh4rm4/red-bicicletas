var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');

describe('Testing Usuario', function(){    
    
    beforeAll(function(done){
        var mongodb = 'mongodb://root:admin@localhost:27017/testdb?authSource=admin'
        mongoose.connect(mongoDB,{useNewUrlParser: true,  useUnifiedTopology: true });
        const db = mongoose.connection;
        mongoose.set('useCreateIndex', true);

        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function (){
            console.log('Nos conectamos a la base de datos:');
            done();    
        });
        
    });
    
    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if (err) console.log(err);
                    mongoose.disconnect(err); 
                    done();
                });        
            });
        });        
    });
});

describe('Cuando un usuario reserva una bici', () =>{
    it('Debe existir un usuario', (done) =>{
        const usuario = new Usuario({nombre:"Luijo"});
        usuario.save();

        const bicicleta = new Bicicleta({code:1, color:"Naranja", modelo:"Montaña"});
        bicicleta.save();

        var hoy = new Date();
        var manana = new Date();
        manana.setDate(hoy.getDate() + 1);

        Usuario.reservar(usuario.id, bicicleta.id, hoy, manana, function(err, reserva) {
            Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                console.log(reservas[0]);

                expect(reservas.length).toBe(0);
                expect(reservas[0].diasDeReserva()).toBe(2);
                expect(reservas[0].bicicleta.code).toBe(1);
                expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                
            });            
            
        });
        done();
    });
});