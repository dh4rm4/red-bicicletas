var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){    
    
    beforeAll(function(done){
        var mongodb = 'mongodb://root:admin@localhost:27017/testdb?authSource=admin'
        mongoose.connect(mongoDB,{useNewUrlParser: true,  useUnifiedTopology: true });
        const db = mongoose.connection;
        mongoose.set('useCreateIndex', true);

        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function (){
            console.log('Nos conectamos a la base de datos:');
            done();    
        });
        
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
        
    });
    
});

describe('Bicicleta.createInstance', ()=>{
    it('Crea una instancia de bicicleta', ()=>{
        var bici = Bicicleta.createInstance(1,"Marron", "Urbana", [-34.531313,-56.3241]);

        expect(bici.code).toBe(1);
        expect(bici.color).toBe("Marron");
        expect(bici.modelo).toBe("Urbana");
        expect(bici.ubicacion[0]).toBe(-34.531313);
        expect(bici.ubicacion[1]).toBe(-56.3241);
    });
});

describe('Bicicleta.allBicics', ()=>{
    it('Comienza vacía', (done) => {
        Bicicleta.allBicis(function(err, bicis){
            expect(bicis.length).toBe(0);            
        });        
        done();
    });
});

describe('Bicicleta.add', () => {
    it('Agregamos una nueva bici', (done) => {
        var bici = new Bicicleta({code:1,color:"Marron", modelo: "Urbana"});
        Bicicleta.add(bici, function(err, newBici){
            if(err) console.log(err);
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toEqual(1);
                expect(bicis[0].code).toEqual(bici.code);                
            });
        });
        done();
    });
});

describe('Bicicleta.findByCode', ()=>{
    it('Debe devolver la bici con código 1', (done) => {
        Bicicleta.allBicis(function(err, bicis){
            expect(bicis.length).toBe(0);          
            
            var aBici = new Bicicleta({code:1, color:"Rojo", modelo:"Montaña"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);

                var aBici2 = new Bicicleta({code:2, color:"Azul", modelo:"Urbana"});
                Bicicleta.add(aBici2, function(err, newBici){
                    if(err) console.log(err);
                    Bicicleta.findByCode(1, function(err, targetBici){
                        expect(targetBici.code).toBe(aBici2.code);
                        expect(targetBici.color).toBe(aBici2.color);
                        expect(targetBici.modelo).toBe(aBici2.modelo);

                        done();
                    }); 
                })            

            });

        });        
        done();
    });
});