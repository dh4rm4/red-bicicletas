var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe('Testing Bicicletas', function(){    
    
    beforeAll(function(done){
        var mongodb = 'mongodb://root:admin@localhost:27017/testdb?authSource=admin';
        mongoose.connect(mongoDB,{useNewUrlParser: true,  useUnifiedTopology: true });
        const db = mongoose.connection;
        mongoose.set('useCreateIndex', true);

        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function (){
            console.log('Nos conectamos a la base de datos:');
            done();    
        });        
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });        
    }); 
});


describe('Bicicleta API', ()=>{
    describe('GET BICICLETA', ()=>{
        it('STATUS 200', ()=>{
            expect(Bicicleta.allBicis.length).toBe(0);

            var bici = new Bicicleta({code:123, color:"Blanca", modelo:"Montaña"}) //new Bicicleta(3,"Marron", "Mont", [-34.531313,-56.3241]);
            Bicicleta.add(bici);
            
            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });
});

describe('POST Bicicletas /create', ()=>{
    it('STATUS 200', (done)=>{
        var headers = {'content-type':'application/json'};
        var bici = '{"id":10, "color":"Blanca", "modelo":"Nuevo","lat":-34.321,"lng":-56.2312}';
        request.post({
            headers:headers,
            url: "http://localhost:3000/api/bicicletas/create",
            body: bici
        },function(error, response, body){
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe("Blanca");
            done();        
        });
    });
});