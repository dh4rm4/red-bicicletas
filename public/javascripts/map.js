var map = L.map('main_map').setView([-37.9809254026193,-57.57453918457031], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap</a>'
      }).addTo(map);



$.ajax({
	dataType: "json",
	url: "api/bicicletas",
	success: function(result){
		console.log(result);
		result.bicicletas.forEach(function(bici){
			L.marker(bici.ubicacion, { title: bici.id}).addTo(map);

		});
	}
})


/*L.marker([-37.9809254026193,-57.57453918457031]).addTo(map);
L.marker([-37.9909254026193,-57.59453918457031]).addTo(map);
L.marker([-37.9709254026193,-57.56453918457031]).addTo(map);*/
