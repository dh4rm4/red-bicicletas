var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;

var usuarioSchema = new Schema({
    nombre: String,
});

usuarioSchema.statics.reservar = function(_id, biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario:_id, bicicleta: biciId, desde:desde, hasta:hasta});
    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.statics.add = function(aUsuario, cb){
    this.create(aUsuario,cb);
};


module.exports = mongoose.model('Usuario', usuarioSchema);