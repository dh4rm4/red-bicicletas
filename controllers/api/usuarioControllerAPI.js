var Usuario = require('../../models/usuario');

exports.usuarios_list = function(req, res){
    Usuario.find({}, function(err, usuarios){
        res.status(200).json({
            usuarios: usuarios
        });
    });
};

/*creacion y borrado*/

exports.usuario_create = function(req, res){
    var usuario = new Usuario({nombre: req.body.nombre});
    
    Usuario.add(usuario);
    res.status(200).json({
        usuario:usuario
    });

};

exports.usuario_reservar = function(req, res){
    Usuario.findById(req.body.id, function(err, usuario){
        console.log(usuario);
        Usuario.reservar(req.body.id, req.body.biciId, req.body.desde, req.body.hasta, function(err){
            console.log("Reserva realizada con éxito!");
            res.status(200).send();
        });
    });
};
